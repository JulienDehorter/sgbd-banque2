package fr.afpa.Affichage;

import java.util.Scanner;

import fr.afpa.Metier.Gestion_BDD;
import fr.afpa.beans.Impression;

public class Affichage {

	/*
	 * Afficher le menu
	 * 
	 */
	public static void Menu() {

		System.out.println(" ");
		System.out.println("1- Cr�er une agence                             ");
		System.out.println("2- Cr�er un client                              ");
		System.out.println("3- Cr�er un compte bancaire                     ");
		System.out.println("4- Rechercher un compte                         ");
		System.out.println("5- Rechercher un client                         ");
		System.out.println("6- Afficher la liste des comptes d'un client    ");
		System.out.println("7- Imprimer les infos d'un client               ");
		System.out.println("8- D�sactiver un client                         ");
		System.out.println("9- Supprimer un compte                          ");
		System.out.println("10- Quitter le programme                        ");
		System.out.println("");

		Scanner sc = new Scanner(System.in);
		int choix = sc.nextInt();

		switch (choix) {
		case 1:
			Gestion_BDD.creerAgence();
			break;
		case 2:
			Gestion_BDD.creerClient();
			break;

		case 3:
			Gestion_BDD.creerCompte();
			break;
		case 4:
			Gestion_BDD.rechercherCompte();
			break;
		case 5:
			Gestion_BDD.rechercherClient();
			break;
		case 6:
			Gestion_BDD.afficherComptesClient();
			break;
		case 7:
			Impression.genererFiche();
			break;
		case 8:
			Gestion_BDD.desactiverClient();
			break;
		case 9:
			Gestion_BDD.supprimerCompte();
			break;
		case 10:
			System.exit(1);
			break;
			
			
			
		}

	}

}
