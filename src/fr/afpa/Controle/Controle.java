package fr.afpa.Controle;

import java.util.regex.Pattern;

public class Controle {

	/*
	 * Verification du format de l'id client
	 * 
	 */
	public static boolean checkIdClient(String idClient) {

		if (Pattern.matches("[A-Z]{2}\\d{6}", idClient)) {

			return true;
		}

		return false;
	}

	/*
	 * Verification du solde du compte. Le client est-t-il ou non � d�couvert ?
	 */

	public static boolean checkDecouvert(int solde) {

		boolean decouvert = solde < 0 ? true : false;
		return decouvert;

	}

}
