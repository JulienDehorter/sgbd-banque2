package fr.afpa.beans;

public class Agence {

	// Attributs
	int codeAgence;
	String nom;
	String adresse;

	// Constructeur
	public Agence(int codeAgence, String nom, String adresse) {
		super();
		this.codeAgence = codeAgence;
		this.nom = nom;
		this.adresse = adresse;
	}

	// Constructeur par d�faut
	public Agence() {
		super();
		// TODO Auto-generated constructor stub
	}

	// Getters and setters
	public int getCodeAgence() {
		return codeAgence;
	}

	public void setCodeAgence(int codeAgence) {
		this.codeAgence = codeAgence;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

}
