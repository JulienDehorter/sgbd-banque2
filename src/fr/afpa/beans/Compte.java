package fr.afpa.beans;

public class Compte {

	// Attributs
	long numCompte;
	float solde;
	boolean decouvert;

	// Constructeur
	public Compte(long numCompte, float solde, boolean decouvert) {
		super();
		this.numCompte = numCompte;
		this.solde = solde;
		this.decouvert = decouvert;
	}

	// Constructeur par d�faut
	public Compte() {
		super();
		// TODO Auto-generated constructor stub
	}

	// Getters and setters
	public long getNumCompte() {
		return numCompte;
	}

	public void setNumCompte(long numCompte) {
		this.numCompte = numCompte;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public boolean isDecouvert() {
		return decouvert;
	}

	public void setDecouvert(boolean decouvert) {
		this.decouvert = decouvert;
	}

}
