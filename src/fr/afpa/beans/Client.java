package fr.afpa.beans;

public class Client {

	// Attributs
	String idClient;
	String nom;
	String prenom;
	String dateNaissance;
	String email;
	boolean actif;

	// Constructeur
	public Client(String idClient, String nom, String prenom, String dateNaissance, String email, boolean actif) {
		super();
		this.idClient = idClient;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.email = email;
		this.actif = actif;
	}

	// Constructeur par d�faut
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}

	// Getters and setters
	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActif() {
		return actif;
	}

	public void setActif(boolean actif) {
		this.actif = actif;
	}

}
