package fr.afpa.Metier;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import fr.afpa.Controle.Controle;

public class Gestion_BDD {

	/*
	 * Cr�ation d'une agence
	 * 
	 */
	public static void creerAgence() {

		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;

		System.out.println("Veuillez entrer le nom de la nouvelle agence");
		Scanner sc = new Scanner(System.in);
		String nomAgence = sc.nextLine();
		System.out.println("Veuillez entrer l'adresse de la nouvelle agence");
		Scanner sc1 = new Scanner(System.in);
		String adresseAgence = sc1.nextLine();

		try { // Charger et configurer le driver de la base de donn�es.
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Test gestion compte", "postgres",
					"azerty01");

			// Requete � executer
			String strQuery = "insert into agence values (nextval ('seqAgence'),'" + nomAgence + "', '" + adresseAgence
					+ "')";
			stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);

			System.out.println("Informations stock�es dans la base de donn�es");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			// Fermeture du statement

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
		}

	}

	/*
	 * Cr�ation d'un client
	 * 
	 */
	public static void creerClient() {

		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		String idClient = null;

		do {
			System.out.println(
					"Veuillez entrer l'id du nouveau client sous la forme AA123456 (attention aux majuscules !)");
			Scanner sc = new Scanner(System.in);
			idClient = sc.nextLine();
		} while (!Controle.checkIdClient(idClient));

		System.out.println("Veuillez entrer le nom du nouveau client");
		Scanner sc = new Scanner(System.in);
		String nom = sc.nextLine();

		System.out.println("Veuillez entrer le pr�nom du nouveau client");
		Scanner sc1 = new Scanner(System.in);
		String prenom = sc1.nextLine();

		System.out.println("Veuillez entrer la date de naissance nouveau client sous la forme yyyy-mm-dd");
		Scanner sc2 = new Scanner(System.in);
		String dateNaissance = sc2.nextLine();

		System.out.println("Veuillez entrer l'email du nouveau client");
		Scanner sc3 = new Scanner(System.in);
		String email = sc3.nextLine();
		boolean bool = true;

		try { // Charger et configurer le driver de la base de donn�es.
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Test gestion compte", "postgres",
					"azerty01");

			// Requete � executer
			String strQuery = "insert into client values ('" + idClient + "', '" + nom + "' , '" + prenom + "', '"
					+ dateNaissance + "', '" + email + "', '" + bool + "')";
			// String strQuery = "insert into client values ('FR859215','" + nom + "', '"
			// +prenom+ "', ' 1988-06-20 ', '" +email+ "', '" +bool+ "')";
			stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);

			System.out.println("Informations stock�es dans la base de donn�es");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			// Fermeture du statement

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
		}

	}

	/*
	 * Cr�ation d'un compte
	 * 
	 */
	public static void creerCompte() {

		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		String idClient = null;

		System.out.println("Veuillez entrer le solde du client");
		Scanner sc = new Scanner(System.in);
		float solde = sc.nextFloat();

		//boolean decouvert = Controle.checkDecouvert(solde);
		System.out.println("Le client a-t-il une autorisation de d�couvert ? True/False");
		Scanner sc0 = new Scanner(System.in);
		boolean decouvert = sc0.nextBoolean();

		
		do {
			System.out.println("Veuillez entrer l'id du client sous la forme AA123456 (attention aux majuscules !)");
			Scanner sc1 = new Scanner(System.in);
			idClient = sc1.nextLine();
		} while (!Controle.checkIdClient(idClient));

		System.out.println("Veuillez entrer l'ID de l'agence du client");
		Scanner sc4 = new Scanner(System.in);
		String idAgence = sc4.nextLine();

		try { // Charger et configurer le driver de la base de donn�es.
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Test gestion compte", "postgres",
					"azerty01");

			// Requete � executer
			String strQuery = "insert into compte values ('" + solde + "', '" + decouvert + "' ,  + nextval('seqCompte') , '"
					+ idClient + "', '" + idAgence + "')";
			// String strQuery = "insert into client values ('FR859215','" + nom + "', '"
			// +prenom+ "', ' 1988-06-20 ', '" +email+ "', '" +bool+ "')";
			stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);

			System.out.println("Informations stock�es dans la base de donn�es");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			// Fermeture du statement

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
		}

	}

	/*
	 * Rechercher un compte via son num�ro
	 * 
	 */
	public static void rechercherCompte() {

	}

	/*
	 * Rechercher un client avec son nom, num�ro de compte, identifiant de client
	 * 
	 */
	public static void rechercherClient() {

	}

	/*
	 * Afficher les comptes d'un client � partir de son id client
	 * 
	 */
	public static void afficherComptesClient() {

	}

	/*
	 * Desactiver un client
	 * 
	 */
	public static void desactiverClient() {

	}

	/*
	 * Supprimer un compte
	 * 
	 */
	public static void supprimerCompte() {

	}

}
